require 'test_helper'

class CourseControllerTest < ActionDispatch::IntegrationTest
  test "should get conceptual_physics" do
    get course_conceptual_physics_url
    assert_response :success
  end

  test "should get algebra_based_physics" do
    get course_algebra_based_physics_url
    assert_response :success
  end

  test "should get calculus_based_physics" do
    get course_calculus_based_physics_url
    assert_response :success
  end

  test "should get calculus" do
    get course_calculus_url
    assert_response :success
  end

  test "should get computer_science" do
    get course_computer_science_url
    assert_response :success
  end

end
