require 'test_helper'

class AboutControllerTest < ActionDispatch::IntegrationTest
  test "should get standards" do
    get about_standards_url
    assert_response :success
  end

  test "should get bio" do
    get about_bio_url
    assert_response :success
  end

end
