class CourseController < ApplicationController
  def conceptual_physics
  end

  def algebra_based_physics
  end

  def calculus_based_physics
  end

  def calculus
  end

  def computer_science
  end
end
