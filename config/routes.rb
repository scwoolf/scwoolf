Rails.application.routes.draw do
  
  root 'home#home'

  get 'course/conceptual_physics'

  get 'course/algebra_based_physics'

  get 'course/calculus_based_physics'

  get 'course/calculus'

  get 'course/computer_science'

  get 'about/standards'

  get 'about/bio'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
